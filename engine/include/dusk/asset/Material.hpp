#ifndef DUSK_MATERIAL_HPP
#define DUSK_MATERIAL_HPP

#include <dusk/Config.hpp>

#include <dusk/asset/Texture.hpp>
#include <dusk/asset/Shader.hpp>
#include <memory>

namespace dusk {

struct MaterialData
{
    alignas(16)  glm::vec4 Ambient  = glm::vec4(0, 0, 0, 1);
    alignas(16)  glm::vec4 Diffuse  = glm::vec4(0, 0, 0, 1);
    alignas(16)  glm::vec4 Specular = glm::vec4(0, 0, 0, 1);

    alignas(4)   GLuint MapFlags = 0;
};

class Material
{
public:

    enum TextureID
    {
        AMBIENT  = 0,
        DIFFUSE  = 1,
        SPECULAR = 2,
        NORMAL   = 3,
    };

    enum MapFlags : GLuint
    {
        AMBIENT_MAP_FLAG  = 1,
        DIFFUSE_MAP_FLAG  = 2,
        SPECULAR_MAP_FLAG = 4,
        NORMAL_MAP_FLAG   = 8,
    };

    struct Data
    {
        glm::vec4 Ambient = glm::vec4(0);
        glm::vec4 Diffuse = glm::vec4(0);
        glm::vec4 Specular = glm::vec4(0);

        std::string AmbientMap = "";
        std::string DiffuseMap = "";
        std::string SpecularMap = "";
        std::string NormalMap = "";
    };

    DISALLOW_COPY_AND_ASSIGN(Material)

    Material(const Data& data);

    virtual ~Material() = default;

    void Bind(Shader * sp);

private:

    MaterialData _shaderData;

    glm::vec4 _ambient;
    glm::vec4 _diffuse;
    glm::vec4 _specular;

    Texture * _ambientMap;
    Texture * _diffuseMap;
    Texture * _specularMap;
    Texture * _normalMap;

};

} // namespace dusk

#endif // DUSK_MATERIAL_HPP
